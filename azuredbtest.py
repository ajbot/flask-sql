from AzureDB import AzureDB

with AzureDB() as a:
    data = a.azureGetData()
    print(type(data))
    for d in data:
        print("%s once said \"%s\"" % (d['name'], d['quote']))