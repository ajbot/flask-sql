from flask import Flask, render_template, request
from AzureDB import AzureDB 

app = Flask(__name__) 
app.config.from_object(__name__) 

@app.route('/') 
def hello(): 
   with AzureDB() as a:
     data = a.azureGetData()       
   return render_template("quotes.html", data = data)


@app.route('/welcome', methods=['POST']) 
def welcome(): 
   return render_template("welcome.html", myName=request.form['myName'])
