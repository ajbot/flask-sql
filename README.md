This is a simple project that demonstrates using pypyodbc to query an Azure SQL Server database with Python3. 

[See the blog post on how it is setup](https://blog.ajbothe.com/querying-an-azure-sql-database-with-python-on-linux-using-pypyodbc)
[See the blog post on using it with Flask](https://blog.ajbothe.com/a-guide-on-flask-and-azure-sql)